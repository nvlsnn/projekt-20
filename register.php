<?php
    include 'config.php';

if(isset($_POST['submit'])){
    $username  = $_POST['username'];
    $email     = $_POST['email'];
    $password  = md5($_POST['password']);  //hash 32-simbols
    $cpassword = md5($_POST['cpassword']);
    
    if($password == $cpassword) {
        $sql = "SELECT * FROM users WHERE email='$email'";
        $result = mysqli_query($conn, $sql);
        if(!$result -> num_rows > 0){
            $sql = "INSERT INTO users (username, email, password)
                    VALUES ('$username', '$email', '$password')";
            $result = mysqli_query($conn, $sql);

            if($result >0) {     
                echo "<script>alert('User Registration Completed.')</script>";
                $username = "";
                $email = "";
                $_POST['password'] = "";
                $_POST['cpassword'] = "";

            } else{
                echo "<script>alert('Something Wrong Matched.')</script>";
            }
        } else {
            echo "<script>alert('Password Not Matched.')</script>";
        }
    }


}


?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous">
    
    <link rel="stylesheet" type="text/css" href="log.css">

    <title>Register Form PHP</title>
</head>
<body>
    <div class="container">
        <form action="" method="POST" class="login-email">
            <p class="login-text">Register</p>
            <div class="input-group">
                <input type="text" placeholder="Username" name="username" required>
            </div>
            <div class="input-group">
                <input type="email" placeholder="Email" name="email"  required>
            </div>
            <div class="input-group">  
                <input type="password" placeholder="Password" name="password" required>
            </div>
            <div class="input-group">  
                <input type="password" placeholder="Confirm Password" name="cpassword" required>
            </div>
            <div class="input-group">
                <button name="submit" class="btn">Sign up</button>
            </div>
            <p class="login-register-text">Have an account? <a href="login.php">Sign in</a></p> 
            <p class="login-register-home"><a href="index.php"><-Back</a></p>
        </form>
    </div>   
</body>
</html>